INTRODUCTION
------------
The TOC Text Filter module makes available a Filter plugin that can be applied
to a given text format. It simply parses the contents of the field for heading
tags and generates a table of contents, which  placed on top of the existing
field content.

REQUIREMENTS
------------
 * No special requirements

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
    https://drupal.org/documentation/install/modules-themes/modules-8
    for further information.

CONFIGURATION
-------------
 * Configure the usage of this filter within a text format in
    Home » Administration » Configuration » Content authoring.

MAINTAINERS
-----------
Current maintainers:
 * Les Peabody (lpeabody) - https://www.drupal.org/u/lpeabody
