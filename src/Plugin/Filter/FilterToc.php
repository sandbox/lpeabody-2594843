<?php
/**
 * @file
 * Contains \Drupal\toc_text_filter\Plugin\Filter\FilterToc.
 */

namespace Drupal\toc_text_filter\Plugin\Filter;

use Drupal\Core\Url;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\Component\Utility\Html;

/**
 * Provides a filter for adding a TOC section to the beginning of a text field.
 *
 * @Filter(
 *   id = "filter_toc",
 *   title = @Translation("Add a TOC to the beginning of the text."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 * )
 */
class FilterToc extends FilterBase {
  /**
   * Performs the filter processing.
   *
   * @param string $text
   *   The text string to be filtered.
   * @param string $langcode
   *   The language code of the text to be filtered.
   *
   * @return \Drupal\filter\FilterProcessResult
   *   The filtered text, wrapped in a FilterProcessResult object, and possibly
   *   with associated assets, cacheability metadata and placeholders.
   *
   * @see \Drupal\filter\FilterProcessResult
   */
  public function process($text, $langcode) {
    $dom = Html::load($text);
    $xpath = new \DOMXPath($dom);
    // Extract all the heading tags from the DOM.
    $query = $xpath->query('//*[self::h1 or self::h2 or self::h3 or self::h4 or self::h5 or self::h6]');
    $toc = '';
    $last_level = 0;
    // Iterate through the heading tags as they occur in the DOM.
    foreach ($query as $index => $child) {
      /** @var $child \DOMElement */
      $level = intval($child->tagName[1]);
      if($level > $last_level) {
        // If the current heading level is greater than the last level, then we
        // need to start adding to a nested list element.
        $toc .= "<ol>";
      }
      else
      {
        // Otherwise we just continue adding to the current list element. If we
        // went from, for example, an h4 to an h1, we need to close off three
        // list elements.
        $toc .= str_repeat('</li></ol>', $last_level - $level);
        $toc .= '</li>';
      }
      $toc .= '<li>' . \Drupal::l($child->textContent, Url::fromUserInput('#toc_' . $index));

      $last_level = $level;
      // Set an id on the heading element so that we can jump to it.
      $child->setAttribute('id', 'toc_' . $index);
    }
    // Close off the TOC.
    $toc .= str_repeat('</li></ol>', $last_level);
    // Serialize the generated DOM which includes the altered heading elments
    // with ids.
    $text = Html::serialize($dom);

    // Return a processed result, with the table of contents prepended to the
    // filtered text up to this point.
    return new FilterProcessResult($toc . $text);
  }
}
